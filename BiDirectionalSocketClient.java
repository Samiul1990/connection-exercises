import java.io.*;
import java.net.*;
import java.util.*;


public class BiDirectionalSocketClient{
	public void go(){
		Scanner scanner = new Scanner(System.in);
		
		try{
			
			while(true){
				/*Socket s = new Socket("127.0.0.1", 4242);
				System.out.println("InetAddress: " + s.getInetAddress());
				System.out.println("Local Address: " + s.getLocalAddress());
				System.out.println("Local Port: " + s.getLocalPort());*/
				
				OutputStream stream = s.getOutputStream();
				PrintWriter writer = new PrintWriter(stream);
				
				InputStreamReader streanReader = new InputStreamReader(s.getInputStream());
				BufferedReader reader = new BufferedReader(streanReader);
				
				System.out.println("Please Enter your input");				
				String write = scanner.nextLine();
				writer.println(write);
				writer.flush();
				
				System.out.println("Request sent");
			
				String response = reader.readLine();
				System.out.println("Response Recevied");
				System.out.println(response);
			
				writer.close();
				
			}
			
			//writer.println(" Hello world");
			
			
		}catch(IOException ex){
			ex.printStackTrace();
		}
	}
	public static void main(String[] args){
		BiDirectionalSocketClient client = new BiDirectionalSocketClient();
		client.go();
	}
}