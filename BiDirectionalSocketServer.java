import java.io.*;
import java.net.*;

public class BiDirectionalSocketServer{
	public void go(){
		try{
			ServerSocket sreverSocket = new ServerSocket(4242);
			while(true){
				Socket socket = sreverSocket.accept();
				
				InputStreamReader streanReader = new InputStreamReader(socket.getInputStream());
				BufferedReader reader = new BufferedReader(streanReader);
				
				OutputStream stream = socket.getOutputStream();
				PrintWriter writer = new PrintWriter(stream);
				
				String request = reader.readLine();
				writer.println(request.toUpperCase());
				writer.flush();
				
				System.out.println("Request received, "+ request);
				System.out.println("Request Sent, "+ request.toUpperCase());
				reader.close();
			}
		}catch(IOException ex){
			ex.printStackTrace();
		}
	}
	public static void main(String[] args){
		BiDirectionalSocketServer server = new BiDirectionalSocketServer();
		server.go();
	}
}
